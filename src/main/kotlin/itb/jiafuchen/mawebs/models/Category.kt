package itb.jiafuchen.mawebs.models

import java.util.*

data class Category(
    val uuid : String,
    var name : String,
    val createDate : Date,
    var updateDate : Date,
)