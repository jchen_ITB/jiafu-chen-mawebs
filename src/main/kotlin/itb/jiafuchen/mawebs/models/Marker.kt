package itb.jiafuchen.mawebs.models

import java.util.*

data class Marker(
    val uuid : String,
    var name : String,
    var categoryID : String?,
    var url : String,
    var read : Boolean,
    val createDate : Date,
    var updateDate : Date,
)
