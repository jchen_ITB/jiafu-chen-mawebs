package itb.jiafuchen.mawebs

import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.response.*
import io.ktor.server.sessions.*
import itb.jiafuchen.mawebs.datasource.postgres.DatabaseFactory
import itb.jiafuchen.mawebs.plugins.configureRouting
import itb.jiafuchen.mawebs.plugins.configureSerialization
import java.util.*

// Change this to "postgres" or "mongo" to switch database

const val DATABASE = "postgres"
//const val DATABASE = "mongo"

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused")
fun Application.module() {
    DatabaseFactory()
    configureSerialization()
    configureRouting()
}

