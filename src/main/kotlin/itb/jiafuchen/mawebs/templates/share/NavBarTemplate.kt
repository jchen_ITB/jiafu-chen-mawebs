package itb.jiafuchen.mawebs.templates.share

import io.ktor.server.html.*
import kotlinx.html.*

class NavBarTemplate() : Template<FlowContent> {
    override fun FlowContent.apply() {
        header {
            nav("navbar navbar-dark bg-dark navbar-expand-lg") {
                div("container-fluid") {
                    button(classes = "navbar-toggler") {
                        type = ButtonType.button
                        attributes["data-mdb-toggle"] = "collapse"
                        attributes["data-mdb-target"] = "#navbarSupportedContent"
                        attributes["aria-controls"] = "navbarSupportedContent"
                        attributes["aria-expanded"] = "false"
                        attributes["aria-label"] = "Toggle navigation"
                        i("fas fa-bars") {
                        }
                    }
                    div("collapse navbar-collapse") {
                        id = "navbarSupportedContent"
                        ul("navbar-nav me-auto mb-2 mb-lg-0") {
                            li("nav-item") {
                                a(classes = "nav-link") {
                                    href = "/"
                                    +"""Inicio"""
                                }
                            }
                            li("nav-item") {
                                a(classes = "nav-link") {
                                    href = "/categories"
                                    +"""Categories"""
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}