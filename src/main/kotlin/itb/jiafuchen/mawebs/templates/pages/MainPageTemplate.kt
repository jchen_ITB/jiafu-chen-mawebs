package itb.jiafuchen.mawebs.templates.pages

import io.ktor.server.html.*
import itb.jiafuchen.mawebs.models.Category
import itb.jiafuchen.mawebs.models.Marker
import itb.jiafuchen.mawebs.templates.share.FooterTemplate
import itb.jiafuchen.mawebs.templates.share.HeadTemplate
import itb.jiafuchen.mawebs.templates.share.NavBarTemplate
import kotlinx.html.*

class MainPageTemplate() : Template<HTML> {

    var markers: List<Marker> = listOf()
    var categories: List<Category> = listOf()
    override fun HTML.apply() {
        head{
            title {
                +"Mawebs - Mis webs"
            }

            insert(HeadTemplate(), TemplatePlaceholder())

        }
        body{
            insert(NavBarTemplate(), TemplatePlaceholder())

            main {
                div("m-5") {
                    div("row") {
                        div("col-lg-12 mt-2") {
                            div("card") {
                                div("card-header") {
                                    ul("nav nav-pills") {
                                        id = "ex1"
                                        role = "tablist"
                                        li("nav-item") {
                                            role = "presentation"
                                            a(classes = "nav-link active") {
                                                id = "ex1-tab-1"
                                                attributes["data-mdb-toggle"] = "pill"
                                                href = "#ex1-pills-1"
                                                role = "tab"
                                                attributes["aria-controls"] = "ex1-pills-1"
                                                attributes["aria-selected"] = "true"
                                                +"""New"""
                                            }
                                        }
                                        li("nav-item") {
                                            role = "presentation"
                                            a(classes = "nav-link") {
                                                id = "ex1-tab-2"
                                                attributes["data-mdb-toggle"] = "pill"
                                                href = "#ex1-pills-2"
                                                role = "tab"
                                                attributes["aria-controls"] = "ex1-pills-2"
                                                attributes["aria-selected"] = "false"
                                                +"""Read"""
                                            }
                                        }
                                    }
                                }
                                div("card-body pb-5") {
                                    div("tab-content") {
                                        id = "ex1-content"
                                        div("tab-pane fade show active") {
                                            id = "ex1-pills-1"
                                            role = "tabpanel"
                                            attributes["aria-labelledby"] = "ex1-tab-1"
                                            div("text-end") {
                                                a(classes = "fs-2 text-primary") {
                                                    style = "cursor: pointer"
                                                    i("far fa-plus-square") {
                                                        attributes["data-mdb-toggle"] = "modal"
                                                        attributes["data-mdb-target"] = "#markerAddModal"
                                                    }
                                                }
                                            }
                                            table("table align-middle mb-0 bg-white") {
                                                thead("bg-white") {
                                                    tr {
                                                        th { +"""Name""" }
                                                        th { +"""URL""" }
                                                        th { +"""Category""" }
                                                        th(classes = "text-center") { +"""Accion""" }
                                                    }
                                                }
                                                tbody {
                                                    markers.filter { !it.read }.forEach { marker ->
                                                        tr {
                                                            td { +marker.name }
                                                            td {
                                                                a(target = "_blank") {
                                                                    href = marker.url
                                                                    +marker.url
                                                                }
                                                            }
                                                            val categoryName = categories.find { it.uuid == marker.categoryID }?.name ?: ""
                                                            td { +categoryName }
                                                            td {
                                                                div("text-center") {
                                                                    a(classes = "text-primary fs-4 mx-2") {
                                                                        href = "/marker/read/${marker.uuid}"
                                                                        type = "button"
                                                                        i("fa-solid fa-check") {
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        div("tab-pane fade") {
                                            id = "ex1-pills-2"
                                            role = "tabpanel"
                                            attributes["aria-labelledby"] = "ex1-tab-2"
                                            table("table align-middle mb-0 bg-white") {
                                                thead("bg-white") {
                                                    tr {
                                                        th { +"""Name""" }
                                                        th { +"""URL""" }
                                                        th { +"""Category""" }
                                                        th(classes = "text-center") { +"""Accion""" }
                                                    }
                                                }
                                                tbody {
                                                    markers.filter { it.read }.forEach { marker ->
                                                        tr {
                                                            td { +marker.name }
                                                            td { +marker.url }
                                                            val categoryName = categories.find { it.uuid == marker.categoryID }?.name ?: ""
                                                            td { +categoryName }
                                                            td {
                                                                div("text-center") {
                                                                    a(classes = "text-danger fs-4 mx-2") {
                                                                        href = "/marker/delete/${marker.uuid}"
                                                                        type = "button"
                                                                        i("fas fa-trash-alt") {
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                div("modal fade") {
                    id = "markerAddModal"
                    tabIndex = "-1"
                    attributes["aria-labelledby"] = "markerAddModalLabel"
                    attributes["aria-hidden"] = "true"
                    div("modal-dialog modal-lg") {
                        div("modal-content") {
                            div("modal-header") {
                                h5("modal-title") {
                                    id = "markerAddModalLabel"
                                    +"""Add web"""
                                }
                                button(classes = "btn-close") {
                                    type = ButtonType.button
                                    attributes["data-mdb-dismiss"] = "modal"
                                    attributes["aria-label"] = "Close"
                                }
                            }
                            div("modal-body") {
                                form {
                                    id = "addWebForm"
                                    action = "/marker/add"
                                    encType = FormEncType.multipartFormData
                                    method = FormMethod.post
                                    div("row") {
                                        div("col-md-8") {
                                            div("form-outline mb-4") {
                                                input(classes = "form-control form-control-lg active") {
                                                    type = InputType.text
                                                    id = "marker_name"
                                                    name = "marker_name"
                                                    placeholder = "My web"
                                                    required = true
                                                }
                                                label("form-label") {
                                                    htmlFor = "web_name"
                                                    style = "margin-left: 0px"
                                                    +"""Name"""
                                                }
                                            }
                                        }
                                        div("col-md-4") {
                                            select("form-select") {
                                                attributes["aria-label"] = "select"
                                                id = "marker_category"
                                                name = "marker_category"
                                                option {
                                                    value = ""
                                                    +"""No category"""
                                                }
                                                categories.forEach {
                                                    option {
                                                        value = it.uuid
                                                        +it.name
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    div("form-outline mb-4") {
                                        textArea(classes = "form-control active") {
                                            id = "marker_url"
                                            name = "marker_url"
                                            rows = "4"
                                            placeholder = "https://www.youtube.com/"
                                            required = true
                                        }
                                        label("form-label") {
                                            htmlFor = "marker_url"
                                            style = "margin-left: 0px"
                                            +"""URL"""
                                        }
                                    }
                                }
                            }
                            div("modal-footer") {
                                button(classes = "btn btn-secondary") {
                                    type = ButtonType.button
                                    attributes["data-mdb-dismiss"] = "modal"
                                    +"""Close"""
                                }
                                button(classes = "btn btn-primary") {
                                    type = ButtonType.submit
                                    form = "addWebForm"
                                    +"""Add"""
                                }
                            }
                        }
                    }
                }
            }

            insert(FooterTemplate(), TemplatePlaceholder())
        }
    }
}