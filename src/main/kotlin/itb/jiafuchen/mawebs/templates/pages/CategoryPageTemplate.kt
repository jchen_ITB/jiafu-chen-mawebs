package itb.jiafuchen.mawebs.templates.pages

import io.ktor.server.html.*
import itb.jiafuchen.mawebs.models.Category
import itb.jiafuchen.mawebs.templates.share.FooterTemplate
import itb.jiafuchen.mawebs.templates.share.HeadTemplate
import itb.jiafuchen.mawebs.templates.share.NavBarTemplate
import kotlinx.html.*

class CategoryPageTemplate() : Template<HTML> {

    var categories : List<Category> = emptyList()
    override fun HTML.apply() {
        head {
            title {
                +"Mawebs - Categories"
            }

            insert(HeadTemplate(), TemplatePlaceholder())
        }

        body {
            insert(NavBarTemplate(), TemplatePlaceholder())

            main {
                div("m-5") {
                    div("row") {
                        div("col-lg-12 mt-2") {
                            div("card") {
                                div("card-body pb-5") {
                                    div("text-end") {
                                        a(classes = "fs-2 text-primary") {
                                            style = "cursor: pointer"
                                            i("far fa-plus-square") {
                                                attributes["data-mdb-toggle"] = "modal"
                                                attributes["data-mdb-target"] = "#categoryAddModal"
                                            }
                                        }
                                    }
                                    table("table align-middle mb-0 bg-white") {
                                        thead("bg-white") {
                                            tr {
                                                th { +"""Name""" }
                                                th(classes = "text-center") { +"""Accion""" }
                                            }
                                        }
                                        tbody {
                                            categories.forEach {
                                                tr {
                                                    td { +it.name }
                                                    td {
                                                        div("d-flex flex-column") {
                                                            div("text-center") {
                                                                a(classes = "text-danger fs-4 mx-2") {
                                                                    href = "/category/delete/${it.uuid}"
                                                                    type = "button"
                                                                    i("fas fa-trash-alt") {
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                div("modal fade") {
                    id = "categoryAddModal"
                    tabIndex = "-1"
                    attributes["aria-labelledby"] = "categoryAddModalLabel"
                    attributes["aria-hidden"] = "true"
                    div("modal-dialog modal-lg") {
                        div("modal-content") {
                            div("modal-header") {
                                h5("modal-title") {
                                    id = "categoryAddModalLabel"
                                    +"""Add category"""
                                }
                                button(classes = "btn-close") {
                                    type = ButtonType.button
                                    attributes["data-mdb-dismiss"] = "modal"
                                    attributes["aria-label"] = "Close"
                                }
                            }
                            div("modal-body") {
                                form {
                                    id = "addCategoryForm"
                                    encType = FormEncType.multipartFormData
                                    method = FormMethod.post
                                    action = "/category/add"
                                    div("col-md-12") {
                                        div("form-outline mb-4") {
                                            input(classes = "form-control form-control-lg active") {
                                                type = InputType.text
                                                id = "category_name"
                                                name = "category_name"
                                                placeholder = "Game"
                                                required = true
                                            }
                                            label("form-label") {
                                                htmlFor = "category_name"
                                                style = "margin-left: 0px"
                                                +"""Name"""
                                            }
                                        }
                                    }
                                }
                            }
                            div("modal-footer") {
                                button(classes = "btn btn-secondary") {
                                    type = ButtonType.button
                                    attributes["data-mdb-dismiss"] = "modal"
                                    +"""Close"""
                                }
                                button(classes = "btn btn-primary") {
                                    type = ButtonType.submit
                                    form = "addCategoryForm"
                                    +"""Add"""
                                }
                            }
                        }
                    }
                }
            }

            insert(FooterTemplate(), TemplatePlaceholder())
        }
    }

}