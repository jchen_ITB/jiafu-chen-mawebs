package itb.jiafuchen.mawebs.datasource.mongo.repository

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.ServerApi
import com.mongodb.ServerApiVersion
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Updates
import itb.jiafuchen.mawebs.datasource.MawebsDatabase
import itb.jiafuchen.mawebs.datasource.mongo.models.CategoryDocument
import itb.jiafuchen.mawebs.datasource.mongo.models.MarkerDocument
import itb.jiafuchen.mawebs.models.Category
import itb.jiafuchen.mawebs.models.Marker
import org.bson.Document
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.pojo.PojoCodecProvider
import org.mongodb.kbson.ObjectId
import java.util.*

const val DATABASE_NAME = "mawebs"
const val MARKER_COLLECTION = "Marker"
const val CATEGORY_COLLECTION = "Category"

class MongoRepository : MawebsDatabase {

    private lateinit var markerCollections: MongoCollection<MarkerDocument>
    private lateinit var categoryCollections: MongoCollection<CategoryDocument>
    private lateinit var client : MongoClient

    init {
        val connection = "mongodb+srv://jchen:jchen@jiafu-chen-spothub-clus.bcydtag.mongodb.net/?retryWrites=true&w=majority";
        val serverAPI = ServerApi.builder()
            .version(ServerApiVersion.V1)
            .build()
        val mongoClientSetting = MongoClientSettings.builder()
            .applyConnectionString(ConnectionString(connection))
            .serverApi(serverAPI)
            .build()
        val pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build()
        val pojoCodecRegistry = CodecRegistries.fromRegistries(
            MongoClientSettings.getDefaultCodecRegistry(),
            CodecRegistries.fromProviders(pojoCodecProvider)
        )

        try{
            client = MongoClients.create(mongoClientSetting)
            client.startSession()

            val db = client.getDatabase(DATABASE_NAME).withCodecRegistry(pojoCodecRegistry)
            db.runCommand(Document("ping", 1))
            println("Pinged your deployment. You successfully connected to MongoDB!")

            markerCollections = db.getCollection(MARKER_COLLECTION, MarkerDocument::class.java)
            categoryCollections = db.getCollection(CATEGORY_COLLECTION, CategoryDocument::class.java)


        }catch (e: Exception) {
            println("Failed to connect to MongoDB Atlas: ${e.message}")
        }
    }

    private fun toDomainMarker(markerDoc: MarkerDocument) : Marker = Marker(
        uuid = markerDoc.marker_id,
        categoryID = markerDoc.categoryID,
        name = markerDoc.name,
        url = markerDoc.url,
        read = markerDoc.read,
        updateDate = Date(markerDoc.updateDate),
        createDate = Date(markerDoc.createDate)
    )

    private fun toDomainCategory(categoryDoc: CategoryDocument) : Category = Category(
        uuid = categoryDoc.category_id,
        name = categoryDoc.name,
        updateDate = Date(categoryDoc.updateDate),
        createDate = Date(categoryDoc.createDate)
    )


    override suspend fun getAllMarkers(): List<Marker> {
        return markerCollections.find().map { toDomainMarker(it) }.toList()
    }

    override suspend fun getMarkerByID(id: String): Marker? {
        val filter = Filters.eq("marker_id", id)
        return markerCollections.find(filter).first()?.let { toDomainMarker(it) }
    }

    override suspend fun addMarker(marker: Marker): Marker? {
        val markerDoc = MarkerDocument().apply {
            _id = ObjectId()
            marker_id = marker.uuid
            categoryID = marker.categoryID ?: ""
            name = marker.name
            url = marker.url
            read = marker.read
            updateDate = marker.updateDate.time
            createDate = marker.createDate.time
        }
        val id = markerCollections.insertOne(markerDoc).insertedId
        return markerCollections.find(Filters.eq("_id", id)).first()?.let { toDomainMarker(it) }
    }

    override suspend fun updateMarker(id: String, newMarker: Marker): Boolean {
        val filter = Filters.eq("marker_id", id)
        val updateMarker = Updates.combine(
            Updates.set("name", newMarker.name),
            Updates.set("categoryID", newMarker.categoryID),
            Updates.set("url", newMarker.url),
            Updates.set("read", newMarker.read),
            Updates.set("updateDate", System.currentTimeMillis())
        )

        val result = markerCollections.updateOne(filter, updateMarker)
        return result.modifiedCount == 1L
    }

    override suspend fun deleteMarker(id: String): Boolean {
        val filter = Filters.eq("marker_id", id)
        val result = markerCollections.deleteOne(filter)
        return result.deletedCount == 1L
    }

    override suspend fun getAllCategories(): List<Category> {
        return categoryCollections.find().map { toDomainCategory(it) }.toList()
    }

    override suspend fun getCategoryByID(id: String): Category? {
        val filter = Filters.eq("category_id", id)
        return categoryCollections.find(filter).first()?.let { toDomainCategory(it) }
    }

    override suspend fun addCategory(category: Category): Category? {
        val categoryDoc = CategoryDocument().apply {
            _id = ObjectId()
            category_id = category.uuid
            name = category.name
        }

        val result = categoryCollections.insertOne(categoryDoc).insertedId

        val newCategory = categoryCollections.find(Filters.eq("_id", result)).first()

        return newCategory?.let { toDomainCategory(it) }
    }

    override suspend fun updateCategory(id: String, newCategory: Category): Boolean {
        val filter = Filters.eq("category_id", id)
        val updateCategory = Updates.combine(
            Updates.set("name", newCategory.name),
            Updates.set("updateDate", System.currentTimeMillis())
        )

        val result = categoryCollections.updateOne(filter, updateCategory)
        return result.modifiedCount == 1L
    }

    override suspend fun deleteCategory(id: String): Boolean {
        val filter = Filters.eq("category_id", id)
        val result = categoryCollections.deleteOne(filter)
        return result.deletedCount == 1L
    }

}