package itb.jiafuchen.mawebs.datasource.mongo.models

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.Index
import io.realm.kotlin.types.annotations.PrimaryKey
import org.mongodb.kbson.ObjectId

class MarkerDocument() : RealmObject {
    @PrimaryKey
    var _id : ObjectId = ObjectId()
    @Index
    var marker_id : String = ""
    var categoryID : String = ""
    var name : String = ""
    var url : String = ""
    var read : Boolean = false
    var createDate : Long = System.currentTimeMillis()
    var updateDate : Long = System.currentTimeMillis()

}