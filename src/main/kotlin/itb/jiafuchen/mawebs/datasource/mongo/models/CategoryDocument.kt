package itb.jiafuchen.mawebs.datasource.mongo.models

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.Index
import io.realm.kotlin.types.annotations.PrimaryKey
import org.mongodb.kbson.ObjectId

class CategoryDocument() : RealmObject {

    @PrimaryKey
    var _id : ObjectId = ObjectId()
    @Index
    var category_id : String = ""
    var name : String = ""
    var createDate : Long = System.currentTimeMillis()
    var updateDate : Long = System.currentTimeMillis()
}