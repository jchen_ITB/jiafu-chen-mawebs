package itb.jiafuchen.mawebs.datasource

import itb.jiafuchen.mawebs.models.Category
import itb.jiafuchen.mawebs.models.Marker


interface MawebsDatabase {

    suspend fun getAllMarkers() : List<Marker>

    suspend fun getMarkerByID(id: String) : Marker?

    suspend fun addMarker(marker : Marker) : Marker?

    suspend fun updateMarker(id: String, newMarker : Marker) : Boolean

    suspend fun deleteMarker(id: String) : Boolean

    suspend fun getAllCategories() : List<Category>

    suspend fun getCategoryByID(id: String) : Category?

    suspend fun addCategory(category: Category) : Category?

    suspend fun updateCategory(id: String, newCategory: Category) : Boolean

    suspend fun deleteCategory(id: String) : Boolean

}