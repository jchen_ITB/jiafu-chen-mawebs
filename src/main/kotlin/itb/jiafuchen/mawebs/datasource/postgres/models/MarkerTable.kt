package itb.jiafuchen.mawebs.datasource.postgres.models

import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

object MarkerTable : Table("marker") {

    val id = varchar("id", 100)
    val categoryID = varchar("category_id", 100).references(CategoryTable.id, onDelete = ReferenceOption.CASCADE, onUpdate = ReferenceOption.CASCADE).nullable()
    val name = varchar("name", 100)
    val url = varchar("url", 2000)
    val read = bool("read")
    val updateDate = long("update_date").default(System.currentTimeMillis())
    val createDate = long("create_date").default(System.currentTimeMillis())

    override val primaryKey = PrimaryKey(id)

}