package itb.jiafuchen.mawebs.datasource.postgres.repository

import itb.jiafuchen.mawebs.datasource.MawebsDatabase
import itb.jiafuchen.mawebs.datasource.postgres.DatabaseFactory.dbQuery
import itb.jiafuchen.mawebs.datasource.postgres.models.CategoryTable
import itb.jiafuchen.mawebs.datasource.postgres.models.MarkerTable
import itb.jiafuchen.mawebs.models.Category
import itb.jiafuchen.mawebs.models.Marker
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import java.util.*

class PostgresRepository : MawebsDatabase {

    private fun resultRowToMarker(row: ResultRow) = Marker(
        uuid = row[MarkerTable.id],
        categoryID = row[MarkerTable.categoryID] ?: "",
        name = row[MarkerTable.name],
        url = row[MarkerTable.url],
        read = row[MarkerTable.read],
        createDate = Date(row[MarkerTable.createDate]),
        updateDate = Date(row[MarkerTable.updateDate])
    )

    private fun resultRowToCategory(row: ResultRow) = Category(
        uuid = row[CategoryTable.id],
        name = row[CategoryTable.name],
        createDate = Date(row[CategoryTable.createDate]),
        updateDate = Date(row[CategoryTable.updateDate])
    )

    override suspend fun getAllMarkers(): List<Marker> = dbQuery {
        MarkerTable.selectAll().map { resultRowToMarker(it) }
    }

    override suspend fun getMarkerByID(id: String): Marker? = dbQuery {
        MarkerTable.select { MarkerTable.id eq id }.mapNotNull {
            resultRowToMarker(it)
        }.singleOrNull()
    }

    override suspend fun addMarker(marker: Marker): Marker? = dbQuery {
        val insertStatement = MarkerTable.insert {
            it[id] = marker.uuid
            if(!marker.categoryID.isNullOrEmpty()){
                it[categoryID] = marker.categoryID
            }
            it[name] = marker.name
            it[url] = marker.url
            it[read] = marker.read
            it[createDate] = marker.createDate.time
            it[updateDate] = marker.updateDate.time
        }

        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToMarker)
    }

    override suspend fun updateMarker(id: String, newMarker: Marker): Boolean = dbQuery {
        MarkerTable.update({ MarkerTable.id eq id }) {
            it[categoryID] = if(newMarker.categoryID.isNullOrEmpty()) null else newMarker.categoryID
            it[name] = newMarker.name
            it[url] = newMarker.url
            it[read] = newMarker.read
            it[updateDate] = newMarker.updateDate.time
        } > 0
    }
    override suspend fun deleteMarker(id: String): Boolean = dbQuery {
        MarkerTable.deleteWhere { MarkerTable.id eq id } > 0
    }

    override suspend fun getAllCategories(): List<Category> = dbQuery {
        CategoryTable.selectAll().map { resultRowToCategory(it) }
    }

    override suspend fun getCategoryByID(id: String): Category? = dbQuery {
        CategoryTable.select { CategoryTable.id eq id }.mapNotNull {
            resultRowToCategory(it)
        }.singleOrNull()
    }

    override suspend fun addCategory(category: Category): Category? = dbQuery {
        val insertStatement = CategoryTable.insert {
            it[id] = category.uuid
            it[name] = category.name
            it[createDate] = category.createDate.time
            it[updateDate] = category.updateDate.time
        }

        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToCategory)
    }

    override suspend fun updateCategory(id: String, newCategory: Category): Boolean = dbQuery{
        CategoryTable.update({ CategoryTable.id eq id }) {
            it[name] = newCategory.name
            it[updateDate] = newCategory.updateDate.time
        } > 0
    }

    override suspend fun deleteCategory(id: String): Boolean = dbQuery {
        CategoryTable.deleteWhere { CategoryTable.id eq id } > 0
    }
}