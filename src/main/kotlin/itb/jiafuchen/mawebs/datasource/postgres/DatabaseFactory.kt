package itb.jiafuchen.mawebs.datasource.postgres


import itb.jiafuchen.mawebs.datasource.postgres.models.CategoryTable
import itb.jiafuchen.mawebs.datasource.postgres.models.MarkerTable
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

object DatabaseFactory {

    val driverClassName = "org.postgresql.Driver"
    val jdbcURL = "jdbc:postgresql://localhost:5432/mawebs"
    lateinit var database : Database

    operator fun invoke(){

        database = Database.connect(jdbcURL, driverClassName, "postgres", "110120130")
        createTables()
    }

    fun reset(){
        transaction(database) {
            SchemaUtils.drop(
                CategoryTable,
                MarkerTable
            )

        }
        createTables()
    }


    private fun createTables(){

        transaction(database) {
            SchemaUtils.create(CategoryTable)
            SchemaUtils.create(MarkerTable)
        }

    }
    suspend fun <T> dbQuery(block: suspend () -> T): T = newSuspendedTransaction(Dispatchers.IO) { block() }

}