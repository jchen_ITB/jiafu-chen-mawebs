package itb.jiafuchen.mawebs.datasource.postgres.models

import org.jetbrains.exposed.sql.Table

object CategoryTable : Table("category") {

    val id = varchar("id", 100)
    val name = varchar("name", 100)
    val updateDate = long("update_date").default(System.currentTimeMillis())
    val createDate = long("create_date").default(System.currentTimeMillis())

    override val primaryKey = PrimaryKey(id)

}