package itb.jiafuchen.mawebs.routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import itb.jiafuchen.mawebs.datasource.MawebsDatabase
import itb.jiafuchen.mawebs.models.Category
import itb.jiafuchen.mawebs.models.Marker
import itb.jiafuchen.mawebs.templates.pages.CategoryPageTemplate
import itb.jiafuchen.mawebs.templates.pages.MainPageTemplate
import java.util.*


fun Route.mainRouting(repository: MawebsDatabase) {

    route("/") {

        get("") {

            val markers = repository.getAllMarkers()
            val categories = repository.getAllCategories()

            val page = MainPageTemplate().apply {
                this.markers = markers
                this.categories = categories
            }
            call.respondHtmlTemplate(page){

            }
        }

        get("categories"){
            val categories = repository.getAllCategories()

            val page = CategoryPageTemplate().apply {
                this.categories = categories
            }

            call.respondHtmlTemplate(page){

            }

        }

        post("category/add") {
            val fd = call.receiveParameters()

            val categoryName = fd["category_name"] ?: return@post call.respondText("Missing or malformed name", status = HttpStatusCode.BadRequest)

            val newCategory = Category(
                uuid = UUID.randomUUID().toString(),
                name = categoryName,
                createDate = Date(),
                updateDate = Date(),
            )

            val result = repository.addCategory(newCategory)

            if(result != null){
                call.respondRedirect("/categories")
            } else {
                call.respondText("Error adding category", status = HttpStatusCode.InternalServerError)
            }
        }

        get("category/delete/{id}") {

            val id = call.parameters["id"] ?: return@get call.respondText("Missing or malformed id", status = HttpStatusCode.BadRequest)

            val category = repository.getCategoryByID(id)
                ?: return@get call.respondText("Category $id not found", status = HttpStatusCode.NotFound)

            val deleted = repository.deleteCategory(id)

            if(deleted){
                call.respondRedirect("/categories")
            } else {
                call.respondText("Error deleting category ${category.uuid}", status = HttpStatusCode.InternalServerError)
            }

        }

        post("marker/add") {
            val fd = call.receiveParameters()

            val productID = fd["marker_name"] ?: return@post call.respondText("Missing or malformed name", status = HttpStatusCode.BadRequest)
            val categoryID = fd["marker_category"] ?: return@post call.respondText("Missing or malformed category", status = HttpStatusCode.BadRequest)
            val url = fd["marker_url"] ?: return@post call.respondText("Missing or malformed url", status = HttpStatusCode.BadRequest)

            val newMarker = Marker(
                uuid = UUID.randomUUID().toString(),
                name = productID,
                categoryID = categoryID,
                url = url,
                read = false,
                createDate = Date(),
                updateDate = Date(),
            )

            val result = repository.addMarker(newMarker)

            if(result != null){
                call.respondRedirect("/")
            } else {
                call.respondText("Error adding marker", status = HttpStatusCode.InternalServerError)
            }

        }

        get("marker/read/{id}") {

            val id = call.parameters["id"] ?: return@get call.respondText("Missing or malformed id", status = HttpStatusCode.BadRequest)

            val marker = repository.getMarkerByID(id)
                ?: return@get call.respondText("Marker $id not found", status = HttpStatusCode.NotFound)

            marker.read = true
            marker.updateDate = Date()

            val updated = repository.updateMarker(id, marker)

            if(updated){
                call.respondRedirect("/")
            } else {
                call.respondText("Error updating marker ${marker.uuid}", status = HttpStatusCode.InternalServerError)
            }

        }


        get("marker/delete/{id}") {

            val id = call.parameters["id"] ?: return@get call.respondText("Missing or malformed id", status = HttpStatusCode.BadRequest)

            val marker = repository.getMarkerByID(id)
                ?: return@get call.respondText("Marker $id not found", status = HttpStatusCode.NotFound)

            val deleted = repository.deleteMarker(id)

            if(deleted){
                call.respondRedirect("/")
            } else {
                call.respondText("Error deleting marker ${marker.uuid}", status = HttpStatusCode.InternalServerError)
            }

        }
    }

}

