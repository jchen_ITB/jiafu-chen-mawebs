package itb.jiafuchen.mawebs.plugins

import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.routing.*
import itb.jiafuchen.mawebs.DATABASE
import itb.jiafuchen.mawebs.datasource.mongo.repository.MongoRepository
import itb.jiafuchen.mawebs.datasource.postgres.repository.PostgresRepository
import itb.jiafuchen.mawebs.routes.mainRouting


fun Application.configureRouting() {

    val repository = when (DATABASE) {
        "postgres" -> PostgresRepository()
        "mongo" -> MongoRepository()
        else -> throw Exception("Invalid database")
    }

    routing {
        println("Routing")
        mainRouting(repository)

        static("/static") {
            resources("files")
        }

    }
}
